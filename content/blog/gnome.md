+++
title = "GNOME"
date = "2018-10-10T13:07:31+02:00"
tags = ["thengOS"]
categories = ["Workshop"] 
author = "Vishnu K" 

+++

## FOSSers of Vidya help translation of GNOME to Malayalam

![Image](/img/portfolio/work1.jpg)

The FOSSers Club of Vidya is a thriving community of free software enthusiasts  in Vidya’s campus. The members of the Club are actively participating in several free software projects. The latest project in the series of such activities is helping in the translation of GNOME into Malayalam language.

The Free Software Community of Kerala had decided to create an operating system for Malayalees. This idea was taken up by different FOSS enthusiasts and FOSS communities of Kerala. The development is going on and the first version of such an operating system called “thengOS” is expected to be released on November 1, the birthday of Kerala state. “thengOS” is a GNU/Linux distribution derived from Ubuntu. (http://thengos.gitlab.io).

GNOME is the default desktop environment of Ubuntu operating system. It’s being used by millions of people. GNOME is already available in Malayalam language. But it’s translation to Malayalam is not complete. And for releasing “thengOS”, a sufficient amount of translations have to be completed. So the FOSSers of Vidya decided to help the process! A workshop on translation was held on 25 October 2018 evening at CSE Project Lab. Members off the Club worked on translations from their homes and they were able to translate more than 10 software packages!

---

## Translation Process

   * GNOME has a Translation website : https://l10n.gnome.org/teams/ml/.
   * Each translator has to create an account and get the translation POT file.
   * This POT file is edited using softwares such as Poedit, Qt Linguist.
   * The newly updated POT file is then uploaded back to GNOME’s website.
   * The file will be reviewed and then after the review, it’s merged to GNOME


Through this, students were able to be a part of one of the world’s most used software, GNOME, and also a part of the new Malayalee operating system, thengOS!

thengOS contributors : http://thengos.gitlab.io/contributors.html

