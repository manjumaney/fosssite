+++
title = "Install Fest Ubuntu 18.04"
date = "2018-08-15T21:49:20+02:00"
tags = ["CSE", "Ubuntu"]
categories = ["Workshops"]
author = "Manjumaney C M"

+++

## FOSSers Club of Vidya conducts Install Fest Ubuntu 18.04 

![Image](/img/install-fest-group.jpg)

The FOSSers Club (Free Open Source Software Users Club) of the College, conduced an Install Fest Ubuntu 18.04 during the evening hours of 13 August 2018 in the Advanced Computer Lab of the CSE Dept. As many as 11 students from S3 B Tech (CSE-B) batch participated in the event. The session was intended to help students familiarise with the technical aspects of installing Ubuntu 18.04 LTS operating system in a dual boot environment with Windows 10. During the session the participants installed Ubuntu in 25 newly purchased Dell Laptops donated by PTA. The session was handled by Mr Shali K R and supported by Mr Sreekumar, Mr Arjun K and Mr Arun Lal.