+++
title = "OpenStreetMapathon"
date = "2018-09-02T21:49:20+02:00"
tags = ["Software Freedom day", "Digital Freedom foundation"]
categories = ["Mapping"]
author = "Manjumaney C M"
+++

## FOSSers of Vidya celebrate Software Freedom Day with Mapathon in Open Street Map Project 

![OSM](/img/mapaton.png)

The FOSSers in association with IEDC conducted an OpenStreetMap Mapathon on 15 September 2018 in the College. It was held as a celebration of Software Freedom Day which is an annual worldwide celebration of Free Software organized by Digital Freedom Foundation. Since 2006 free software enthusiasts have been observing the third Saturday of September as Software Freedom Day. As many as forty students from S3 B Tech (CSE, EEE, EC and ME) batches participated in the program.

_OpenStreetMap (OSM)_ is a collaborative project to create a free editable map of the world. Unlike Google Maps, OSM is completely open. It’s data can be accessed and used by anyone. In the recent Kerala Floods, technology was used extensively to help in relief efforts. Two websites may cited in this connection, namely,  keralarescue.in and microid.in/keralaflood. Both of them used OSM. But many places in Kerala are still not mapped. Had the entire Kerala been mapped, Kerala would have been more prepared in facing the calamities of the flood.

The Mapathon was held jointly with OSM Kerala, FSUG Thrissur (thrissur.fsug.in), Kole Birders (kole.org.in) and River Research Centre. The team focused on the area of Chalakkudy and Mukundapuram Taluks. In collective efforts the participants could make 4,400+ contributions to OSM in less than 5 hours (see http://www.missingmaps.org/leaderboards/#/fossersvast for details.)

Manoj Karingamadathil, an alumnus of Vidya who is a Free Software Activist, a Wikipedia/OpenStreetMap volunteer and also a Coordinator of Kole Birders Collective, led the Mapathon. Along with him, Ajith Kumar (GIS Expert from CSRD), Rajaneesh P (River Research Centre), Sujin N S (Jawarhalal Nehru Tropical Botanical Garden, Trivandrum) and Sreenath K (Soil Test expert) joined the program. Naveen Francis (Open Street Map and Wikimedia India) gave technical help for the event. Jaison Nedumbala (Asst. Secretary, Koorachundu Village Panchayat and Member of Swathandra Malayalam Computing) gave advice to mapping efforts.
