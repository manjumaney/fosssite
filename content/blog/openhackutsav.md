+++
title = "OpenHackUtsav"
date = "2018-10-03T21:49:20+02:00"
tags = ["Hacktoberfest", "GitHub"]
categories = ["Workshop"]
author = "Manjumaney C M"

+++

## FOSSers conducts OpenHackUtsav
![Hacktoberfest](/img/portfolio/work2.jpg)


Hacktoberfest, the month-long celebration of open source software, was proudly celebrated in the College under the title OpenHackUtsav on 15 October 2018. The long-awaited program was organized in the MCA Lab, hosted by the FOSSers Club of the College in association with IEDC and Student Developers Society (SDS), Thrissur. It consisted of a Workshop on the basics of git and GitHub. Students of Vidya (belonging to different branches and years) and other colleges like Govt Engg College, Thrissur, and Thejus Engg. College, Vellarakkad, were the participants for the program. 

---
![Hacktoberfest](/img/hackerfest.jpg)

_Hacktoberfest is a month-long celebration of open source software run by DigitalOcean in partnership with GitHub and Twilio. DigitalOcean is an American cloud infrastructure provider headquartered in New York City with data centers worldwide. GitHub is a web-based hosting service for version control using Git. It is mostly used for computer code. Twilio is a cloud communications platform as a service company based in San Francisco, California._

---
The event began with an introduction to FOSS (Free and Open Source Software) provided by Dr V N Krishnachandran (Head, MCA Dept) emphasizing the importance of FOSS, especially in a student’s life. This was followed by a small introductory session to Hacktoberfest and its relevance by Subin Siby (S3 B Tech CSE). Levi Vaguez representing SDS, Thrissur explained its working in a brief talk. After that, the workshop was handled by Mr Ambady Anand S (Software Developer, Free Software Enthusiast, and Privacy Advocate) and his team consisting of Ranjith Siji, Kannan V M and Mujeeb.

During the first half of the workshop, the participants were familiarised with the basic commands of git and functions of Github. Next, the participants were divided into groups and assigned the task of collectively creating a website (about FOSS) to let them understand the practical relevance of GitHub and to get hands-on experience. Mr Biswas (S7 B TEch CSE student from Govt Engg College, Palakkad), the brain behind keralarescue.in, gave an overview of the working of his brilliant project along with the problems they faced during its realization. keralarescue.in is one of the the best examples of how Open Source Collaboration can help people and make an impact. The talk was focused on inspiring the students, setting an example of how youth can change society for the better.

