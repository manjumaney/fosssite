+++
title = "WordPress for Beginners"
date = "2018-10-03T21:49:20+02:00"
tags = ["CSE", "WordPress"]
categories = ["Workshops"]
author = "Manjumaney C M"
+++

## “WordPress for Beginners” workshop : An initiative of FOSSers Club 

![Image](/img/Wp-for-Beginners.jpg)

The FOSSers Club (Free and Open Source Software Users Club) of the College, in association with Innovation & Entrepreneurship Development Cell (IEDC) of the College, organised a one-day programme titled “WordPress for Beginners” on 27 September 2018. Fifteen students from various departments including members of the Web Development Team of the “Minor Research Project VAST” participated in the programme. The programme was intended to familiarise the participants with WordPress which is a free and open-source content management system, blog tool and publishing platform.