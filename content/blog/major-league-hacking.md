+++
title = "Major League Hacking"
date = "2018-12-12T00:00:00Z"
tags = ["CSE"]
categories = ["Workshops"]
author = "Manjumaney C M"

+++

## FOSSers of Vidya participate in Major League Hacking event at Adi Shankara and win prizes 


![Image](/img/mlh-group.jpeg)

They are really passionate about Free and Open Source Software (FOSS)!

As many as thirteen members of the FOSSers’ Club of Vidya participated in the Local Hack Day events organised by the ASIET Hack Club  at Adi Shankara Institute of Engineering and Technology (ASIET), Kaladi, on 4 December 2018. The event was part of the 200+ Local Hack Day events worldwide and was sponsored by GitHub, Microsoft, Twilio and Kerala Startup Mission. The events of the day included hackathons and workshops. The aim was to promote open source software and culture of collaboration, and to introduce people to new technology.

---

![MLH](/img/mlh.jpg)

 _Local Hack Day is organized by Major League Hacking (MLH). Local community volunteers are responsible for organizing the individual locations of Local Hack Day around the world. Major League Hacking, officially abbreviated as MLH, is a company founded in 2013 that operates a league for student hackathons._   

---

All participants were given stickers, swag from GitHub and Major League Hacking. They were also provided with  free US$ 100 credit to Microsoft Azure Cloud platform and access to GitHub education pack for free.

Four of the FOSSers from Vidya participated in the hackathon, The others attended the workshops on web development, news and weather API integration, Dialogflow. As part of the hackathon, the team from Vidya built a project using Hugo, GitHub and Azure Cloud to showcase Linux distributions.

In the web development workshop, a small hackathon was conducted and Liya Derby (S3 B Tech CSE B) won the third prize! She was awarded a USB Drive sponsored by AdaCore University and a certificate from GitHub! Radhika Sharma (S3 B Tech CSE B) and Abhijith Sheheer (S3 B tech CSE A) participated in the GitHub MyOctocat challenge. Octocat is the GitHub mascot.

![octocat](/img/octocat.jpeg)

---

## Participants


    Liya Derby, S3 B Tech CSE B
    Manjumaney C M, S3 B Tech CSE B
    Shikha Sajan, S3 B Tech CSE B
    Subin Siby, S3 B Tech CSE B
    Vishnu K, S3 B Tech CSE B

    Abhijith Sheheer, S1 B Tech CSE A
    Mohammed Hashim, S1 B Tech CSE B
    Radhika Sharma, S1 B Tech CSE B
    Sreelakshmi T.S, S1 B Tech CSE B
    Sruthi I.R, S1 B Tech CSE B
    Vaisakh Unnikrishnan, S1 B Tech CSE B
    Vinayak, S1 B Tech CSE B
    Yadhukrishnan, S1 B Tech CSE B

---
## Gallery

[![IMAGE](/img/blog/m1.jpg)](/img/blog/mlh1.jpg) [![IMAGE](/img/blog/m2.jpg)](/img/blog/mlh2.jpg) [![IMAGE](/img/blog/m3.jpg)](/img/blog/mlh3.jpg) [![IMAGE](/img/blog/m4.jpg)](/img/blog/m4.jpg) [![IMAGE](/img/blog/m5.jpeg)](/img/blog/mlh5.jpeg)   




