+++
title = "GNU/Linux Install Fest Ver. 2.0 "
date = "2017-11-22T00:00:00Z"
tags = ["CSE", "IEDC","ME"]
categories = ["Workshops"]
author = "Vishnu K"

+++

## FOSSers Club, jointly with IEDC, holds GNU/Linux Install Fest Ver. 2.0 

![Image](/img/gnullf.jpg)

FOSSers in association with IEDC, conduced a GNU/Linux Install Fest during the evening hours of 16.11.2017 in the PG Lab of the CSE Dept. Around 15 students from various Departments participated in the event. The FOSSers Club has been organising a series of such Fests in the College in recent days. The sessions are intended to help students familiarise with the technical niceties of installing Ubuntu 14.04 LTS operating system in a dual boot environment with Windows 10. In the latest edition of the Fest, Ubuntu was installed in as many as 20 machines.   As part of the Fest, the participants were given a basic introduction to FOSS concepts. As a preparation for the Fest, a practice session on VirtualBox had also been given to the participants on 15.11.2017. The resource person for the program was Mr. Shali K R. He was supported by Mr. Sreekumar, Mr. Sreejith and Mr. Arjun.